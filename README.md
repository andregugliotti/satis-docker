# Satis Package Manager on Docker

This project configures an instance of Satis Package Manager to be used as Docker container. From the scratch it must be used behind a Traefik proxy but you can configure it to be used along your favorite proxy server, like Nginx.


## How to use this repository

First, you must create two files: _/satis/satis.json_ and _/satis/auth.json_. You can use the *.sample files as start point. Further instructions can be found on official Satis project page.

To deploy the stack, launch this command on the server:

`docker-compose config | docker stack deploy --compose-file - satis`


## Contribution

This is an open source module and can be used as a base for other modules. If you have suggestions for improvements, 
just submit your pull request.

## Versioning

We use SemVer to versioning. To view all versions for this module, visit the tags page, on this repository.

## Authors

Andre Gugliotti - Initial development - [AndreGugliotti](https://gitlab.com/andregugliotti)

Lorenzo Calamandrei - Initial development - [Lorenzo Calamandrei](https://gitlab.com/lorenzocalamandrei)

See also the developers list, with all those who contributed to this project.
 [Contributors](https://gitlab.com/andregugliotti/docker-swarm-proxy-and-monitoring/-/project_members)

## License

This project is licensed under GNU General Public License, version 3.